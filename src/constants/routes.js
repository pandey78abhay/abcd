export const HOME = { link: "/" };
export const FEATURES = { name: "POKEMON CARD", link: "/" };
export const PRODUCTS = { name: "LIST OF POKEMON", link: "/LISTOFPOKEMON" };
export const CUSTOMERS = { name: "DETAILS", link: "/DETAILS" };
export const SALES = { name: "Sales", link: "/sales" };
export const LOGIN = { name: "Login", link: "/login" };
