import React from "react"
//import Statement, { GetStartedButton } from "../../components/features/Statement"
import  { useState, useEffect } from 'react';
//import { withRouter } from "react-router-dom";
import {useHistory} from "react-router-dom"

const Card = (props) => {
    console.log(props)
    let history = useHistory();
    // const[abilities,setabilities]=useState([]);
    // const [forms,setforms]= useState([]);
    // const[height,setheight]= usestate("");
    // const [held_items,setheld_items]= useState([]);
    // const[ids,setids]= useState("");
    // const[stats,setstats]= useState([]);
    // const[types,settypes]= useState([]);
    const[height,setheight]= useState("");
    const[order,setorder]= useState("");
    const[weight,setweight]= useState("");
    const [spi,setspi]= useState({});
    const[load,isload]= useState(true);
    let apiurl= "https://pokeapi.co/api/v2/pokemon/"+props.name;

    // const [poke, setpoke] = useState([]);
    // const[name,setname] = useState("");
    useEffect(() => {
        if(props.name) {
        async function postData(url=apiurl, data = {}) {
            // Default options are marked with *
            const response = await fetch(url, {
              method: 'GET', // *GET, POST, PUT, DELETE, etc.
              mode: 'cors', // no-cors, *cors, same-origin
              cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
              credentials: 'same-origin', // include, *same-origin, omit
              headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
              },
              redirect: 'follow', // manual, *follow, error
              referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
              //body: JSON.stringify(data) // body data type must match "Content-Type" header
            });
            return response.json(); // parses JSON response into native JavaScript objects
          }
          
          postData(apiurl)
            .then(data => {
              console.log(data);
              setspi(data.sprites)
              setheight(data.height)
              setorder(data.order)
              setweight(data.weight)
              //isload(true)
              //setpoke(data.results);
              // JSON data parsed by `data.json()` call
            });
         }
    },[props.name])

    
    
    
        // const onSearch=()=>{
        //     console.log(name)
        // }  
        const abc =()=>{
            console.log("Abhay")
            history.push('/');
            window.location.reload()
        }
    return(
    <div>
        {/* <Statement />
        <GetStartedButton /> */}
        <br/>
      {/* {isload && spi.map((val,key)=>
      <div key={key}>
          <img src={val.back_default}/>
          </div>
      )} */}
       {spi.back_default && <div style={{width:'100%'}}>
           <div style={{width:'100%',backgroundColor:'rebeccapurple',color:'white',fontWeight:'bolder'}}>
           <center><h1  className="col-sm-3">Height: {height}</h1></center>
           <center><h1>Order :{order}</h1></center>
           <center><h1>Weight: {weight}</h1></center>
           </div><br/>
           <div style={{border:'11px solid grey',textDecoration:'underline',color:'black',fontWeight:'bolder',backgroundColor:'antiquewhite'}}>
        <center><h1 >Black Default</h1></center>
      <img style={{width:"100%"}} src ={spi.back_default} />
      </div>
      <br/>
      <div style={{border:'11px solid darkred',textDecoration:'underline',color:'black',fontWeight:'bolder',backgroundColor:'darksalmon'}}>
      <center><h2>Black Shiny</h2></center>
      <img style={{width:"100%"}} style={{width:"100%"}} src ={spi.back_shiny} />
      </div>
      <br/>
      <div style={{border:'11px solid midnightblue',textDecoration:'underline',color:'black',fontWeight:'bolder',backgroundColor:'coral'}}>
      <center><h1>Front Default</h1></center>
      <img style={{width:"100%"}}  src ={spi.front_default} />
      </div>
      <br/>
      <div style={{border:'11px solid indigo',textDecoration:'underline',color:'black',fontWeight:'bolder',backgroundColor:'lightcyan'}}>
      <center><h1>Front shiny</h1></center>
      <img style={{width:"100%"}} src ={spi.front_shiny} />
      </div>
      <br/><br/><br/>
      <center><button className="btn btn-primary" style={{border:'2px solid black',width:'48%',height:'41px',backgroundColor:'red',color:'white',fontWeight:'bolder',borderRadius:'84px'}} onClick={()=>abc()}>Back</button></center>
      <br/><br/><br/>
      </div>}
      
      <br/><br/><br/>

      

    </div>
    );
    }
export default (Card);