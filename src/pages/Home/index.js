import React from "react"
//import Statement, { GetStartedButton } from "../../components/features/Statement"
import  { useState, useEffect } from 'react';
import { withRouter } from "react-router-dom";
import Card from './Card';


const Home = (props) => {
    console.log(props)
    const [poke, setpoke] = useState([]);
    const[name,setname] = useState("");
    const[loading,isloading]= useState("");
    useEffect(() => {
        async function postData(url = 'https://pokeapi.co/api/v2/pokemon?limit=100&offset=200', data = {}) {
            // Default options are marked with *
            const response = await fetch(url, {
              method: 'GET', // *GET, POST, PUT, DELETE, etc.
              mode: 'cors', // no-cors, *cors, same-origin
              cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
              credentials: 'same-origin', // include, *same-origin, omit
              headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
              },
              redirect: 'follow', // manual, *follow, error
              referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
              //body: JSON.stringify(data) // body data type must match "Content-Type" header
            });
            return response.json(); // parses JSON response into native JavaScript objects
          }
          
          postData('https://pokeapi.co/api/v2/pokemon?limit=100&offset=200')
            .then(data => {
              console.log(data.results);
              setpoke(data.results);
              // JSON data parsed by `data.json()` call
            });
    },[])

    
    // const barch=(ev)=>{
    //     console.log(ev)
    // }
    
        // const onSearch=()=>{
        //     console.log(name)
        // }  
    return(
        <>
       
            
    <div className="mt-10 flex flex-col  pb-24">
        {/* <Statement />
        <GetStartedButton /> */}
         <table className="table table-striped">
            <thead >
                
                <tr><center style={{textDecoration:'underline',backgroundColor:'rebeccapurple',border:'1px solid',fontWeight:'bolder',color:'white',height:'45px',margin:'auto',padding:'10px'}}>Name of Pokemon</center></tr>
            </thead>
            </table>
        <br/>
        <div className="form-control">
        {/* <form className="mr-32 w-1/4
                            xs:text-base xs:ml-2 xs:mr-1 xs:w-full
                            sm:text-base sm:ml-3 sm:mr-2 sm:w-2/5 
                            md:text-base md:ml-3 md:mr-2 md:w-2/5 
                            lg:text-base lg:ml-4 md:mr-4 lg:w-1/4"  
                            > */}
        <input
        style={{width:'90%', border :'5px solid blue'}}
        className=" form-control
                   py-2 focus:shadow-outline font-helvetica outline-none
                   rounded-full ml-5 px-5 font-medium placeholder-gray-400
                   tracking-wide w-full"
        type="text"
        name="search"
        placeholder="Search Pokemon"
        aria-label="Search"
        aria-required="false"
        onChange={event=>{setname(event.target.value)}}
    />
    
    {/* </form> */}
        </div>
        
       {poke.filter((val)=>{
           if(name === ""){
               
               return val;
           }
           else if(val.name.toLowerCase().includes(name.toLocaleLowerCase())){
               
               return val;
           }
           
       }).map((val,key)=>
           <tr><center key={key}><h1 >{val.name}</h1></center></tr>
       )}
       

   {/* {loading && <Card name={name} loading={loading}/>} */}
   
       

    </div>
    {name && <Card name={name} /> }
    </>
    );
    }
export default withRouter(Home);