import React from "react"
import { Link } from "react-router-dom"
import { HOME } from "../../../constants/routes"

const Logo = () => (
    <Link to={HOME.link} className="font-bold text-white text-2xl ">
        <span  style={{width:'124px'}}className="flex flex-col bg-brand  w-130 rounded-full justify-center items-center text-center cursor-pointer
        ">
            PokeApi
        </span>
    </Link>)

export default Logo